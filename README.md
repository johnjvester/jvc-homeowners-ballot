# `jvc-homeowners-ballot` Repository

> The `jvc-homeowners-ballot` repository is focused on establishing a web3 smart contract using [Infura](https://infura.io/), 
> [The Truffle Framework](https://www.trufflesuite.com/), [Solidity](https://solidity.readthedocs.io/en/v0.7.1/), 
> [hdwallet](https://www.npmjs.com/package/@truffle/hdwallet-provider) and [Ganache](https://www.trufflesuite.com/ganache).

![Smart Contract Example](ConsenSysSmartContract.png)


## Publications

This repository is related to a DZone.com publication:

* [Moving From Full-Stack Developer To Web3 Pioneer](https://dzone.com/articles/moving-from-full-stack-developer-to-web3-pioneer)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Using This Repository

At a high level, the following steps are required to utilize this repository:

1. Create a new project in Infura
2. `truffle -init`
3. `npm install --save @truffle/hdwallet-provider`
4. `ganache-cli` 
5. `truffle console` followed by:
    * `const HDWalletProvider = require('@truffle/hdwallet-provider');`
    * `const mnemonic = '12 words here';`
    * `const wallet = new HDWalletProvider(mnemonic, "http://localhost:8545");`
6. Add ETH funds for testing
7. `npm install --save dotenv` (and establish `.env` file as noted below)
8. `truffle migrate --network ropsten`

### `.env` File

An `.env` file needs to be created in the root of the project, which includes the following information:

```shell
INFURA_API_KEY=INSERT YOUR API KEY HERE (no quotations)
MNEMONIC="12 words here"
```

## Next Steps

To see how a [React](https://reactjs.org/) client can be created to interact with this smart contact, please review the following repository:

[jvc-homeowners-ballot-client ](https://gitlab.com/johnjvester/jvc-homeowners-ballot-client)


## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
